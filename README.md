# sm_memoryutils

A standalone copy of SourceMod's IMemoryUtils

```
Usage:
  include memoryutils.h
  define PLATFORM_LINUX (or WINDOWS or OSX)
  compile with -std=gnu++11 if using g++
```
